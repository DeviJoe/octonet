FROM node:17-alpine
WORKDIR /tmp/build/
COPY ./frontend/ .
RUN export NODE_OPTIONS=--openssl-legacy-provider && npm install && npm run build

FROM adoptopenjdk/openjdk15-alpine
WORKDIR /tmp/compile/
COPY ./ .
COPY --from=0 /tmp/build/dist/ ./src/main/resources/static/
RUN ./gradlew --no-daemon build -x test

FROM adoptopenjdk/openjdk15-alpine
WORKDIR /app
RUN apk --no-cache add libpcap
COPY --from=1 /tmp/compile/build/libs/octonet-*-SNAPSHOT.jar app.jar
EXPOSE 65000:65000