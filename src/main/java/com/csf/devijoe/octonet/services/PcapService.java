package com.csf.devijoe.octonet.services;

import com.csf.devijoe.octonet.models.Session;
import com.csf.devijoe.octonet.models.enums.SubscriptionMessageType;
import com.csf.devijoe.octonet.models.pojo.SubscriptionMessage;
import com.csf.devijoe.octonet.pcap.NoOpPcapWorker;
import com.csf.devijoe.octonet.pcap.PcapWorker;
import lombok.extern.slf4j.Slf4j;
import org.pcap4j.core.PcapNativeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
@Slf4j
public class PcapService {

    private boolean started = false;

    private final SubscriptionService subscriptionService;
    private final PcapWorker worker;

    @Autowired
    public PcapService(SubscriptionService subscriptionService, PcapWorker worker) {
        this.subscriptionService = subscriptionService;
        this.worker = worker;
    }

    public boolean isStarted() {
        return started || worker instanceof NoOpPcapWorker;
    }

    public synchronized void start() throws PcapNativeException {
        if(!started) {
            started = true;
            subscriptionService.broadcast(new SubscriptionMessage(SubscriptionMessageType.PCAP_STARTED, null));
            worker.start();
        }
    }

    public void updateFilter(Collection<Session> services) {
        String filter;

        if (services.isEmpty()) {
            filter = "tcp or udp";
        } else {
            final String ports = services.stream()
                    .map(Session::getPort)
                    .map(p -> "port " + p)
                    .collect(Collectors.joining(" or "));

            final String format = "(tcp or udp) and (%s)";
            filter = String.format(format, ports);
        }

        log.debug("New filter: " + filter);

        worker.setFilter(filter);
    }

    public String getExecutorState() {
        return worker.getExecutorState();
    }

}
