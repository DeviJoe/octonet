package com.csf.devijoe.octonet.controllers;

import com.csf.devijoe.octonet.models.pojo.StreamDto;
import com.csf.devijoe.octonet.models.pojo.StreamPagination;
import com.csf.devijoe.octonet.services.StreamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/stream/")
public class StreamController {

    private final StreamService service;

    @Autowired
    public StreamController(StreamService service) {
        this.service = service;
    }

    @PostMapping("/all")
    public List<StreamDto> getStreams(@RequestBody StreamPagination pagination) {
        return service.findAll(pagination, Optional.empty(), pagination.isFavorites()).stream()
                .map(service::streamToDto)
                .collect(Collectors.toList());
    }

    @PostMapping("/{port}")
    public List<StreamDto> getStreams(@PathVariable int port, @RequestBody StreamPagination pagination) {
        return service.findAll(pagination, Optional.of(port), pagination.isFavorites()).stream()
                .map(service::streamToDto)
                .collect(Collectors.toList());
    }

    @PostMapping("/{id}/favorite")
    public void favoriteStream(@PathVariable long id) {
        service.setFavorite(id, true);
    }

    @PostMapping("/{id}/unfavorite")
    public void unfavoriteStream(@PathVariable long id) {
        service.setFavorite(id, false);
    }

}
