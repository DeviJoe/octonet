package com.csf.devijoe.octonet.controllers;

import com.csf.devijoe.octonet.models.Session;
import com.csf.devijoe.octonet.models.pojo.ServiceDto;
import com.csf.devijoe.octonet.services.ServicesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/service/")
public class ServiceController {

    private final ServicesService service;

    @Autowired
    public ServiceController(ServicesService service) {
        this.service = service;
    }

    @GetMapping
    public List<ServiceDto> getServices() {
        return service.findAll().stream()
                .map(service::toDto)
                .collect(Collectors.toList());
    }

    @DeleteMapping("/{port}")
    public void deleteService(@PathVariable int port) {
        service.deleteByPort(port);
    }

    @PostMapping
    public Session addService(@RequestBody ServiceDto dto) {
        Session newService = this.service.fromDto(dto);
        return this.service.save(newService);
    }

}
