package com.csf.devijoe.octonet.controllers;

import com.csf.devijoe.octonet.models.Packet;
import com.csf.devijoe.octonet.models.pojo.PacketDto;
import com.csf.devijoe.octonet.models.pojo.PacketPagination;
import com.csf.devijoe.octonet.services.StreamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/packet/")
public class PacketController {

    private final StreamService streamService;

    @Autowired
    public PacketController(StreamService streamService) {
        this.streamService = streamService;
    }

    @PostMapping("/{streamId}")
    public List<PacketDto> getPacketsForStream(@PathVariable long streamId, @RequestBody PacketPagination pagination) {
        List<Packet> packets = streamService.getPackets(streamId, pagination.getStartingFrom(), pagination.getPageSize());
        return packets.stream()
                .map(streamService::packetToDto)
                .collect(Collectors.toList());
    }

}
