package com.csf.devijoe.octonet.controllers;

import com.csf.devijoe.octonet.services.SubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

@SuppressWarnings("NullableProblems")
@Component
public class WebSocketHandler extends TextWebSocketHandler {

    private final SubscriptionService subscriptionService;

    @Autowired
    public WebSocketHandler(SubscriptionService subscriptionService) {
        this.subscriptionService = subscriptionService;
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) {
        subscriptionService.addSubscriber(session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
        subscriptionService.removeSubscriber(session);
    }
}
