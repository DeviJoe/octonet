package com.csf.devijoe.octonet.repository;


import com.csf.devijoe.octonet.models.Session;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiceRepository extends JpaRepository<Session, Integer> {
}
