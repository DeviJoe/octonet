package com.csf.devijoe.octonet.repository;


import com.csf.devijoe.octonet.models.Pattern;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PatternRepository extends JpaRepository<Pattern, Integer> {
}
