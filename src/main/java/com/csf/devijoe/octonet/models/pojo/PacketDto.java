package com.csf.devijoe.octonet.models.pojo;

import com.csf.devijoe.octonet.models.FoundPatternDto;
import lombok.Data;

import java.util.Set;

@Data
public class PacketDto {

    private Long id;
    private Set<FoundPatternDto> matches;
    private long timestamp;
    private boolean incoming;
    private boolean ungzipped;
    private boolean webSocketParsed;
    private boolean tlsDecrypted;
    private byte[] content;

}
