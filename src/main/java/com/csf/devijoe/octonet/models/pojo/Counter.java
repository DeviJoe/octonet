package com.csf.devijoe.octonet.models.pojo;

import lombok.Getter;

@Getter
public class Counter {

    private int value = 0;

    public void increment() {
        value++;
    }

    public void increment(int num) {
        value += num;
    }

}
