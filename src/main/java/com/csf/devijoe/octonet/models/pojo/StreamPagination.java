package com.csf.devijoe.octonet.models.pojo;

import com.csf.devijoe.octonet.models.Pattern;
import lombok.Data;
import org.jetbrains.annotations.Nullable;

@Data
public class StreamPagination {

    @Nullable
    private Long startingFrom;

    private int pageSize;

    private boolean favorites; // определяет, искать только избранные стримы или все

    @Nullable
    private Pattern pattern; // если не null, ищем стримы с этим паттерном

}
