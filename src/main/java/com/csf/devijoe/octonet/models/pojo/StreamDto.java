package com.csf.devijoe.octonet.models.pojo;

import com.csf.devijoe.octonet.models.enums.Protocol;
import lombok.Data;

import java.util.Set;

@Data
public class StreamDto {

    private Long id;
    private int service;
    private Protocol protocol;
    private long startTimestamp;
    private long endTimestamp;
    private Set<PatternDto> foundPatterns;
    private boolean favorite;
    private int ttl;
    private String userAgentHash;

}
