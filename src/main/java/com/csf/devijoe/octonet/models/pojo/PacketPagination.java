package com.csf.devijoe.octonet.models.pojo;

import lombok.Data;
import org.jetbrains.annotations.Nullable;

@Data
public class PacketPagination {

    @Nullable
    private Long startingFrom;

    private int pageSize;

}
