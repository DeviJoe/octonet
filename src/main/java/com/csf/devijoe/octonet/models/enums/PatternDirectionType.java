package com.csf.devijoe.octonet.models.enums;

public enum PatternDirectionType {
    INPUT,
    OUTPUT,
    BOTH
}

