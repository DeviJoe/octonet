package com.csf.devijoe.octonet.models.pojo;

import com.csf.devijoe.octonet.models.enums.PatternActionType;
import com.csf.devijoe.octonet.models.enums.PatternDirectionType;
import com.csf.devijoe.octonet.models.enums.PatternSearchType;
import lombok.Data;

@Data
public class PatternDto {

    private int id;
    private boolean enabled;
    private String name;
    private String value;
    private String color;
    private PatternSearchType searchType;
    private PatternDirectionType directionType;
    private PatternActionType actionType;
    private Integer serviceId;

}
