package com.csf.devijoe.octonet.models.pojo;

import lombok.Data;

@Data
public class ServiceDto {

    private int port;
    private String name;
    private boolean decryptTls;
    private boolean processChunkedEncoding;
    private boolean unGzipHttp;
    private boolean urlDecodeHttpRequests;
    private boolean mergeAdjacentPackets;
    private boolean parseWebSockets;

}
