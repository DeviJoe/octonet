package com.csf.devijoe.octonet.models.pojo;

import com.csf.devijoe.octonet.models.enums.SubscriptionMessageType;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SubscriptionMessage {

    private SubscriptionMessageType type;
    private Object value;

}