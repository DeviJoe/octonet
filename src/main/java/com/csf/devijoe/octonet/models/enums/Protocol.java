package com.csf.devijoe.octonet.models.enums;

public enum Protocol {
    TCP,
    UDP
}
