package com.csf.devijoe.octonet.models;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "service")
public class Session {

    @Id
    private Integer port;

    @Column(nullable = false)
    private String name;

    private boolean decryptTls;

    private boolean processChunkedEncoding;

    private boolean unGzipHttp;

    private boolean urlDecodeHttpRequests;

    private boolean mergeAdjacentPackets;

    private boolean parseWebSockets;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Session that = (Session) o;
        return port != null && Objects.equals(port, that.port);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
