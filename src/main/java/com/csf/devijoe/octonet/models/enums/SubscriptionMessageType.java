package com.csf.devijoe.octonet.models.enums;

public enum SubscriptionMessageType {
    SAVE_SERVICE, SAVE_PATTERN,
    DELETE_SERVICE, DELETE_PATTERN,
    NEW_STREAM,
    FINISH_LOOKBACK,
    COUNTERS_UPDATE,
    ENABLE_PATTERN, DISABLE_PATTERN,
    PCAP_STARTED, PCAP_STOPPED
}
