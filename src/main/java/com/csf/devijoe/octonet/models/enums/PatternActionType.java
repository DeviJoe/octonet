package com.csf.devijoe.octonet.models.enums;

public enum PatternActionType {
    FIND,
    IGNORE
}
