package com.csf.devijoe.octonet.models.enums;


public enum PatternSearchType {
    REGEX,
    SUBSTRING,
    SUBBYTES
}
