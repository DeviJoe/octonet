package com.csf.devijoe.octonet.models;

import lombok.Data;

@Data
public class FoundPatternDto {

    private int patternId;
    private int startPosition;
    private int endPosition;

}
