package com.csf.devijoe.octonet.models.enums;

public enum CaptureMode {
    LIVE,
    FILE,
    VIEW
}
