package com.csf.devijoe.octonet.pcap;

import com.csf.devijoe.octonet.models.enums.Protocol;
import org.pcap4j.core.PcapNativeException;

public interface PcapWorker {

    void start() throws PcapNativeException;
    void stop();

    /**
     * Выполняется в вызывающем потоке
     */
    void closeAllStreams(Protocol protocol);

    /**
     * Выполняется в потоке обработчика
     */
    int closeTimeoutStreams(Protocol protocol, long timeoutMillis);

    void setFilter(String filter);

    String getExecutorState();
}
