package com.csf.devijoe.octonet.pcap;

import com.csf.devijoe.octonet.models.enums.Protocol;
import org.pcap4j.core.PcapNativeException;

public class NoOpPcapWorker implements PcapWorker {
    @Override
    public void start() throws PcapNativeException {
    }

    @Override
    public void stop() {
    }

    @Override
    public void closeAllStreams(Protocol protocol) {
    }

    @Override
    public int closeTimeoutStreams(Protocol protocol, long timeoutMillis) {
        return 0;
    }

    @Override
    public void setFilter(String filter) {
    }

    @Override
    public String getExecutorState() {
        return "none";
    }
}
